import 'dart:convert';

Test testFromJson(String str) => Test.fromJson(json.decode(str));

class Test {
  Test({
    required this.results,
  });

  List<Result> results;

  factory Test.fromJson(Map<String, dynamic> json) => Test(
        results:
            (json["results"] as List).map((e) => Result.fromJson(e)).toList(),
      );
}

class Result {
  Result({
    this.name,
    this.location,
    this.email,
  });

  Name? name;
  Location? location;
  String? email;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        name: Name.fromJson(json["name"]),
        location: Location.fromJson(json["location"]),
        email: json["email"],
      );
}

class Location {
  Location({
    this.coordinates,
  });

  Coordinates? coordinates;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        coordinates: Coordinates.fromJson(json["coordinates"]),
      );
}

class Coordinates {
  Coordinates({
    this.latitude,
    this.longitude,
  });

  String? latitude;
  String? longitude;

  factory Coordinates.fromJson(Map<String, dynamic> json) => Coordinates(
        latitude: json["latitude"],
        longitude: json["longitude"],
      );
}

class Name {
  Name({
    this.title,
    this.first,
    this.last,
  });

  String? title;
  String? first;
  String? last;

  factory Name.fromJson(Map<String, dynamic> json) => Name(
        title: json["title"],
        first: json["first"],
        last: json["last"],
      );
}

List<Test> modelFromJson(String str) =>
    List<Test>.from(json.decode(str).map((x) => Test.fromJson(x)));
