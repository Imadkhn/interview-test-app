// ignore_for_file: avoid_unnecessary_containers

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test_app/json.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text('My Interview Task');

  TextEditingController editingController = TextEditingController();

  void clearText() {
    editingController.clear();
  }

  Future<List<Map<String, dynamic>>>? test;
  @override
  void initState() {
    super.initState();
    test = getData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: customSearchBar,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              onPressed: () {
                setState(() {
                  if (customIcon.icon == Icons.search) {
                    customIcon = const Icon(Icons.cancel);
                    customSearchBar = ListTile(
                      leading: const Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 28,
                      ),
                      title: TextField(
                        onTap: clearText,
                        onChanged: (value) => setState(
                          () {
                            var s = editingController;
                            s;
                          },
                        ),
                        controller: editingController,
                        decoration: const InputDecoration(
                          hintText: 'Search by names...',
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontStyle: FontStyle.italic,
                          ),
                          border: InputBorder.none,
                        ),
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  } else {
                    customIcon = const Icon(Icons.search);
                    customSearchBar = const Text('My Interview Task');
                  }
                });
              },
              icon: customIcon,
            ),
          ],
        ),
        body: Container(
          child: FutureBuilder(
            future: test,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                var results = snapshot.data;
                return SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: results!.length,
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(top: 16),
                      itemBuilder: (context, index) {
                        final list = results![index];
                        if (editingController.text.isEmpty) {
                          return Card(
                            color: Colors.amber[100],
                            margin: const EdgeInsets.all(15),
                            child: Padding(
                              padding: const EdgeInsets.all(16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    list['name']['first'],
                                    // ignore: prefer_const_constructors
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.green),
                                  ),
                                  Text(list['email'].toString()),
                                  Text(
                                    list['location']['street'].toString(),
                                  ),
                                ],
                              ),
                            ),
                          );
                        } else if (list['name']['first']
                                .toLowerCase()
                                .contains(editingController.text) ||
                            list['email'].contains(editingController.text)) {
                          return Card(
                            color: Colors.blueGrey[100],
                            margin: const EdgeInsets.all(15),
                            child: Padding(
                              padding: const EdgeInsets.all(16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${list['name']['first']}',
                                    // ignore: prefer_const_constructors
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.green),
                                  ),
                                  Text('${list['email']}'),
                                  Text('${list['location']['street']}'),
                                ],
                              ),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      }),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Future<List<Map<String, dynamic>>>? getData() async {
    final http.Response httpResponse = await http.get(
        Uri.parse('https://randomuser.me/api/?results=25'),
        headers: {'Content-Type': 'application/json'});

    if (httpResponse.statusCode != 200) null;
    return List<Map<String, dynamic>>.from(
        json.decode(httpResponse.body)['results']);

    // var _json = httpResponse.statusCode == 200 ? httpResponse.body : '--:(===';
    // var testList = testFromJson(_json);
    // return testList;
  }
}
